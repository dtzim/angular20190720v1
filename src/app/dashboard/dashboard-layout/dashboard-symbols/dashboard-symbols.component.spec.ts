import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSymbolsComponent } from './dashboard-symbols.component';

describe('DashboardSymbolsComponent', () => {
  let component: DashboardSymbolsComponent;
  let fixture: ComponentFixture<DashboardSymbolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSymbolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSymbolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
