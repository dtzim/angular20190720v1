import { TestBed } from '@angular/core/testing';

import { SymbolsServiseService } from './symbols-servise.service';

describe('SymbolsServiseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SymbolsServiseService = TestBed.get(SymbolsServiseService);
    expect(service).toBeTruthy();
  });
});
