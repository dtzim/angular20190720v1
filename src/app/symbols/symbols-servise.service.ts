import { WatchList } from './symbol-layout/watchcList';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Symbols } from './symbol-layout/Symbols';

@Injectable({
  providedIn: 'root'
})
export class SymbolsServiseService {

	constructor(private http: HttpClient) {}

	fetchSymbols(): Observable<Symbols[]> {
		//return this.http.get<Symbols[]>('https://localhost:44309/api/Symbols');
		return this.http.get<Symbols[]>('https://localhost:44309/api/Stocks');
	}

	fetchWatchList(): Observable<WatchList[]> {
		return this.http.get<WatchList[]>('https://localhost:44309/api/Watchlists');
	}
}
