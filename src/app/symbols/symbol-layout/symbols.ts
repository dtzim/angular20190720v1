
export class Symbols {
	Id: string;
	Symbol: string;
	Description: string;
	Currency: string;
	StockExchange: String;
	Price: number;
	DateCreated: Date;
	CreatedById: String;
	DateUpdated: Date;
	UpdatedById: String
}
// public int Id { get; set; }
// public String Symbol { get; set; }
// public String Description { get; set; }
// public String Currency { get; set; }
// public String StockExchangeLong { get; set; }
// public float Price { get; set; }
// public DateTime DateCreated { get; set; }
// public String CreatedById { get; set; }
// public DateTime DateUpdated { get; set; }
// public String UpdatedById { get; set; }
