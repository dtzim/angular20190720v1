import { Component, OnInit, OnDestroy } from '@angular/core';

import { SymbolsServiseService } from '../../symbols-servise.service';

@Component({
	selector: 'mw-symbols-watch-list',
	templateUrl: './symbols-watch-list.component.html',
	styleUrls: ['./symbols-watch-list.component.scss']
})
export class SymbolsWatchListComponent implements OnInit {

	isFetching = false;
	arrayWatchList = [];

	constructor(private symbolService: SymbolsServiseService) { }

	ngOnInit() {
		this.isFetching = true;
		this.symbolService.fetchWatchList().subscribe(res => {
			this.arrayWatchList = res;
		})
		this.isFetching = false;
	}

	ngOnDestroy() {
		//this.subscription.unsubscribe();
	}

}
