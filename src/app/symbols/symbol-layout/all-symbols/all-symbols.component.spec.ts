import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSymbolsComponent } from './all-symbols.component';

describe('AllSymbolsComponent', () => {
  let component: AllSymbolsComponent;
  let fixture: ComponentFixture<AllSymbolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllSymbolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSymbolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
