import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SymbolsServiseService } from '../../symbols-servise.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'mw-all-symbols',
	templateUrl: './all-symbols.component.html',
	styleUrls: ['./all-symbols.component.scss']
})
export class AllSymbolsComponent implements OnInit, OnDestroy {

	isFetching = false;
	arraySymbols = [];
	subscription: Subscription;

	constructor(private http: HttpClient, private symbolService: SymbolsServiseService) { }

	ngOnInit() {
		this.isFetching = true;
		this.subscription = this.symbolService.fetchSymbols().subscribe(res => {
			this.arraySymbols = res;
		});
		this.isFetching = false;
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

}
