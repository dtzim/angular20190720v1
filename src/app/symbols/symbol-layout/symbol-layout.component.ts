import { Component, OnInit, OnDestroy } from '@angular/core';

import { HttpClient, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

import { SymbolsServiseService } from './../symbols-servise.service';
import { Symbols } from './symbols';

@Component({
	selector: 'mw-symbol-layout',
	templateUrl: './symbol-layout.component.html',
	styleUrls: ['./symbol-layout.component.scss']
})

export class SymbolLayoutComponent implements OnInit, OnDestroy {

	// subscription: Subscription;
	// symbol$: Observable<Symbols>;
	// arrSymbols: Symbols[] = [];
	// isFetching = false;
	// arrSymbols1: Observable<Symbols>;
	// arrSymbols2 = [];
	// arraySymbols = [];


	constructor(private http: HttpClient, private route: ActivatedRoute, private sybmolService: SymbolsServiseService) { }

	// login() {
	// 	this.sybmolService.fetchSymbols().subscribe(res => {
	// 		this.arraySymbols = res;
	// 	})
	// }

	ngOnInit() {
		// this.arraySymbols = this.http.get<Symbols>('https://localhost:44309/api/Symbols');
		// this.sybmolService.fetchSymbols().subscribe(res => {
		// 	this.arraySymbols = res;
		// })
	}


	ngOnDestroy() {
		// this.subscription.unsubscribe();
	}

}

